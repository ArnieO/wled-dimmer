/*
Node controlling the level of one white LED stripe by inverted PWM
MQTT subscription (receives LED level setting)
    Topic:  /openHAB/out/KjokkenUnderskapDimmer/W where W is the letter of the color channel.

To be done:
MQTT publish (returns LED level setting)
    Topic:  /openHAB/in/KjokkenUnderskapDimmer/W where W is the letter of the color channel.
    
MQTT payload is a string containing a value between 0 (off) and 100 (fully on)
The node electronics inverts the GPIO lines.
The ESP is controlled by values between 0 and 1023.
Due to the inversion, 0 = fully ON / 1023 = fully OFF

              0-100                  1023-0
MQTT Server    -->    ESP firmware    -->     ESP module

Input brightness values 0-100 are converted to gamma corrected, inverted GPIO values 1023-0 by lookup in PWNMTable
Control signals: 
W: GPIO 2

Example on using MQTT client PubSubClient on ESP8266: https://gist.github.com/igrr/7f7e7973366fc01d6393
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>  //MQTT client http://knolleary.net/arduino-client-for-mqtt/

const char *ssid =	   "Grannestien 9";		// cannot be longer than 32 characters!
const char *pass =	   "onomatopoetikon";		//
const char *clientName =   "Stueseksjon";
const String topic =        "/openHAB/out/KjokkenUnderskapDimmer/";

// Update these with values suitable for your network.
IPAddress server(192, 168, 0, 123);
IPAddress ip(192,168,0,128); //Node static IP
IPAddress gateway(192,168,0,1);
IPAddress subnet(255,255,255,0);

//GPIO pin mapping
#define GPIOW 2  //White
#define FADEDURATION 500  //ms
#define FADESTEPS    100

//Gamma correction lookup table.
//The ESP-03 seems to become unstable if it gets the values 1021 and 1022, so those are removed manually from the lookup-table.
int PWMTable[101] = {1023, 1023, 1023, 1023, 1023, 1023, 1020, 1020, 1020, 1019, 1019, 
1018, 1018, 1017, 1015, 1014, 1012, 1011, 1009, 1007, 1005, 1002, 1000, 997, 994, 991, 987, 984, 980, 
976, 972, 968, 963, 959, 954, 949, 943, 937, 932, 925, 919, 913, 906, 899, 891, 884, 876, 868, 859, 
851, 842, 833, 823, 813, 803, 793, 782, 772, 760, 749, 737, 725, 713, 700, 687, 674, 661, 647, 632, 
618, 603, 588, 573, 557, 541, 524, 507, 490, 473, 455, 437, 419, 400, 381, 361, 341, 321, 301, 280, 
258, 237, 215, 192, 170, 146, 123, 99, 75, 50, 25, 0}; 

//Initialization of LEDs
int OldW=50;  //percent

//MQTT client initialization
PubSubClient client(server);  

////////////////////////////////////////////////////////////////
// Fade WHITE LED channel from PWMvalue fromVal to PWMvalue toVal
// PWMvalue range: 0 (ON) to 1023 (OFF)
// Fadeduration: FADEDURATION ms
////////////////////////////////////////////////////////////////
void fadeLEDW(int fromVal, int toVal) {
  float stepW = (float)(abs(fromVal - toVal))/FADESTEPS;
  Serial.print("fromVal: ");
  Serial.print(fromVal);
  Serial.print("     toVal: ");
  Serial.println(toVal);
  for (float n=0 ; n <= FADESTEPS; n++) {
    analogWrite(GPIOW, int((float)fromVal+n*stepW*(((fromVal - toVal) < 0) - ((fromVal - toVal) > 0))));
    delay(int(FADEDURATION / FADESTEPS));
  } 
}

void callback(const MQTT::Publish& pub) {
  Serial.print(pub.topic());
  Serial.print(" => ");
  Serial.println(pub.payload_string());
  Serial.println(PWMTable[(String(pub.payload_string())).toInt()]);
  /*
  for (int n=0 ; n<3 ; n++) {
    if pub.payload_string()[n] {
  */  
    
  String payload = String(pub.payload_string());

  if (pub.topic().endsWith("W")) {
    Serial.print("Setting W to ");
    Serial.println(payload.toInt());
    fadeLEDW(PWMTable[OldW], PWMTable[payload.toInt()]);
    OldW = payload.toInt();
  }
}


void setup()
{
  //Start LEDs at initial brightness
  analogWrite(GPIOW, PWMTable[OldW]);
  
  // Setup console
  Serial.begin(115200);
  delay(10);
  Serial.println();
  Serial.println();

  client.set_callback(callback);

  WiFi.begin(ssid, pass);
  WiFi.config(ip, gateway, subnet);

  //Wifi connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("WiFi connected, using IP address: ");
  Serial.println(WiFi.localIP());

  //MQTT broker connection
  if (client.connect(clientName)) {
    String SubscribeString;
    SubscribeString = topic + "#";
    Serial.println("Connected to MQTT broker.");
    Serial.print("Subscribing to: ");
    Serial.println(SubscribeString);
    
    //Subscribe to topic
    client.subscribe(SubscribeString);
    
    if (client.publish(topic, "hello from ESP8266")) {
      Serial.println("Publish ok");
    }
    else {
      Serial.println("Publish failed");
    }
  }
  else {
    Serial.println("MQTT connect failed");
    Serial.println("Will reset and try again...");
    abort();
  }
}

void loop()
{
  // Re-establish MQTT connection if broken
  if (client.connect(clientName)) {
    String SubscribeString;
    SubscribeString = topic + "#";
    //Subscribe to topic
    client.subscribe(SubscribeString);
    }
  
  //MQTT client loop function
  client.loop();
  
}

