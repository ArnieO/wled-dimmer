-- Dimmer for hvit LED stripe 
-- OpenHAB MQTT
-- Intensitet angis som tall fra 0 til 100, i trinn på 10
-- PWM-kode til ESP gis som invertert og gammakorrigert via tabellen PWMtable
-- Node is inverting

--Init 
DeviceID="KjokkenUnderskapDimmer"  
Broker="192.168.0.123"
PWMTable = {1023, 1020, 1005, 972, 919, 842, 737, 603, 437, 237, 0}

--SSID & PW
SSID = "Grannestien 9"
PW = "onomatopoetikon"
IPADR = "192.168.0.128"
IPROUTER = "192.168.0.1"

--GPIO2 is connected to LED, initially PWM'ing at low level (inverted)
OldBrightness = 50
pwm.setup(4, 100, PWMTable[OldBrightness/10+1]) 
pwm.start(4) 

--Initiate Wifi
wifi.setmode(wifi.STATION)
wifi.sta.setip({ip=IPADR,netmask="255.255.255.0",gateway=IPROUTER})
wifi.sta.config(SSID,PW)

 m = mqtt.Client("ESP8266", 180, "pi", "raspberry")  
 m:lwt("/lwt", "ESP8266", 0, 0)  
 m:on("offline", function(con)   
    print ("Mqtt Reconnecting...")   
    tmr.alarm(1, 10000, 0, function()  
      m:connect(Broker, 1883, 0, function(conn)   
        print("Mqtt Connected to:" .. Broker)  
        mqtt_sub() --run the subscription function  
      end)  
    end)  
 end)  
  
 -- on publish message receive event  
 m:on("message", function(conn, topic, data)   
    print("Recieved:" .. topic .. ":" .. data)   
    if (data=="INCREASE") then  
      if (OldBrightness <=90) then
            OldBrightness = OldBrightness + 10
      end
      print("Increasing LED to " .. OldBrightness .. "%")
      pwm.setduty(4, PWMTable[OldBrightness/10+1])
      m:publish("/openHAB/in/" .. DeviceID .. "/state",tostring(OldBrightness),0,0)
    elseif (data=="DECREASE") then  
      if (OldBrightness >=10) then
            OldBrightness = OldBrightness - 10
      end
      print("Decreasing LED to " .. OldBrightness .. "%") 
      pwm.setduty(4, PWMTable[OldBrightness/10+1])
      m:publish("/openHAB/in/" .. DeviceID .. "/state",tostring(OldBrightness),0,0)
    else  
      print("Invalid - Ignoring")   
    end   
 end)  
 function mqtt_sub()  
    m:subscribe("home/openHAB/out/" .. DeviceID .. "/command",0, function(conn)   
      print("Mqtt Subscribed to OpenHAB feed for device " .. DeviceID)  
    end)  
 end  
 tmr.alarm(0, 1000, 1, function()  
  if wifi.sta.status() == 5 and wifi.sta.getip() ~= nil then  
    tmr.stop(0)  
    m:connect(Broker, 1883, 0, function(conn)   
      print("Mqtt Connected to:" .. Broker)  
      mqtt_sub() --run the subscription function  
    end)  
  end  
 end)  
